
/*Author: Shane Kenyon
 *Program: ShuffleMoveFiller
 */

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener; // library imports
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import java.awt.Robot;
import java.awt.event.KeyEvent;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;

public class ShuffleMoveFiller implements ActionListener {   //creates class for the application and inherits the actionListener interface
	
	JPanel panel;
	JFrame frame;
	JLabel grid1;
	JLabel lblNewLabel = new JLabel("");
	JProgressBar progressBar = new JProgressBar();
	JTextField textField;
	JLabel lblEnterHotkeyFor = new JLabel("Enter Hotkey for this pokemon:");
	
	public static boolean firstTimeThrough = true;
	public static boolean checkPokemon = false;
	
	public static int numberOfChars = 0;
	
	public static String lastChar;
	
	public static String firstSpace;
	public static String firstA;
	public static String firstS;
	public static String firstE;
	public static String firstR;
	
	public static String firstG;
	public static String firstD;
	public static String firstW;
	public static String firstT;
	public static String firstQ;
	public static String firstB;
	
	public static String firstAF;
	public static String firstSF;
	public static String firstEF;
	public static String firstRF;
	
	public static String firstGF;
	public static String firstDF;
	public static String firstWF;
	public static String firstTF;
	public static String firstQF;
	public static String firstBF;
	
	public static List<String> SpaceList = new ArrayList<String>();
	public static List<String> AList = new ArrayList<String>();
	public static List<String> SList = new ArrayList<String>();
	public static List<String> EList = new ArrayList<String>();
	public static List<String> RList = new ArrayList<String>();
	
	public static List<String> GList = new ArrayList<String>();
	public static List<String> DList = new ArrayList<String>();
	public static List<String> WList = new ArrayList<String>();
	public static List<String> TList = new ArrayList<String>();
	public static List<String> QList = new ArrayList<String>();
	public static List<String> BList = new ArrayList<String>();
	
	
	public static List<String> AFList = new ArrayList<String>();
	public static List<String> SFList = new ArrayList<String>();
	public static List<String> EFList = new ArrayList<String>();
	public static List<String> RFList = new ArrayList<String>();
	
	public static List<String> GFList = new ArrayList<String>();
	public static List<String> DFList = new ArrayList<String>();
	public static List<String> WFList = new ArrayList<String>();
	public static List<String> TFList = new ArrayList<String>();
	public static List<String> QFList = new ArrayList<String>();
	public static List<String> BFList = new ArrayList<String>();
	
	public static List<String> finalCharList = new ArrayList<String>();
	
	static int keyInput[] = { KeyEvent.VK_A, KeyEvent.VK_S, KeyEvent.VK_E,
            KeyEvent.VK_R, KeyEvent.VK_G, KeyEvent.VK_D, KeyEvent.VK_W, KeyEvent.VK_T, KeyEvent.VK_Q, KeyEvent.VK_B, KeyEvent.VK_F, KeyEvent.VK_SPACE}; // F is index 10 
	
	static String Keys[] = { 
			"A", "S", "E", "R", "G","D","W","T","Q",
			"AF", "SF", "EF", "RF", "GF","DF","WF","TF","QF", "space"
	};
	
	public static List<String> ActiveKeys = new ArrayList<String>();
	
	
	public static List<String> Paths = new ArrayList<String>(){{
		 for (int i = 1; i <= 36; i++) {
			 add("hash" + i + ".png");
		}
		 
	 }};
	
	public ShuffleMoveFiller() {
		
		frame=new JFrame();   
		panel=new JPanel();
		button.setBounds(238, 427, 107, 23);
		
		frame.setTitle("ShuffleMoveFiller");  //Sets the title of the applications window and also its size
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(600,500);
		frame.setVisible(true);
		frame.getContentPane().add(panel,BorderLayout.CENTER);
		panel.setLayout(null);
		panel.add(button);
		
		
		progressBar.setBounds(117, 387, 332, 14);
		panel.add(progressBar);
		progressBar.setStringPainted(true);
		progressBar.setValue(0);
		File file = new File("C:\\SS\\screen.png");
		lblNewLabel.setBounds(76, 27, 411, 302);
		panel.add(lblNewLabel);
		
		
		lblEnterHotkeyFor.setEnabled(false);
		lblEnterHotkeyFor.setBounds(222, 340, 188, 14);
		panel.add(lblEnterHotkeyFor);
		
		JLabel lblCurrentGrid = new JLabel("Current Grid:");
		lblCurrentGrid.setBounds(253, 2, 153, 14);
		panel.add(lblCurrentGrid);
		
		textField = new JTextField();
		textField.setEnabled(false);
		textField.setBounds(242, 356, 86, 20);
		panel.add(textField);
		textField.setColumns(10);
		
		btnOk.setEnabled(false);
		btnOk.setBounds(338, 355, 89, 23);
		panel.add(btnOk);
	}
	
	JButton button = new JButton( new AbstractAction("Fill ShuffleMove") {
		@Override
	public void actionPerformed(ActionEvent a)  
	{
		progressBar.setString("5% - attempting to take screenshot of Pokemon Shuffle");
		progressBar.setValue(5);
		progressBar.update(progressBar.getGraphics());
		File file = new File("C:\\SS\\screen.png");
		file.delete();
		try {
			Runtime.getRuntime().exec("C:\\SS\\Screenshot.bat");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		while(!screenshotPhone()){
			try {
			    Thread.sleep(100);               
			} catch(InterruptedException ex) {
			    Thread.currentThread().interrupt();
			}
		}
		progressBar.setString("8% - Screenshot Taken");
		progressBar.setValue(8);
		progressBar.update(progressBar.getGraphics());
		try {
			Image orig = ImageIO.read(new File("C:\\SS\\screen.png"));
			int x = 30, y = 947, w = 1380, h = 1395;
			BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
			bi.getGraphics().drawImage(orig, 0, 0, w, h, x, y, x + w, y + h, null);
			progressBar.setString("10% - Attempting to crop grid");
			progressBar.setValue(10);
			ImageIO.write(bi, "png", new File("C:\\SS\\screen_cropped.png"));
			File f = new File("C:\\SS\\screen_cropped.png");
			ImageIcon icon = new ImageIcon("C:\\SS\\screen_cropped.png"); 
			progressBar.setString("15% - Grid cropped and loaded");
			progressBar.setValue(15);
			progressBar.update(progressBar.getGraphics());
			 Image dip = ImageIO.read(f);
			 lblNewLabel.setText("");
			 lblNewLabel.setIcon(new ImageIcon(dip.getScaledInstance(411, 302, 411)));
			int w2 = bi.getWidth();
			int h2 = bi.getHeight();	
//			int[] dataBuffInt = bi.getRGB(0, 0, w2, h2, null, 0, w2);
//			Color mycolour = new Color(dataBuffInt[100]);
			progressBar.setString("18% - Splitting grid into individual squares");
			progressBar.setValue(18);
			progressBar.update(progressBar.getGraphics());
			int w3 = 225, h3 = 240, x3= 0, y3 = 0;
			BufferedImage img1 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img1.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img1, "png", new File("C:\\SS\\img1.png"));
			int w4 = 225, h4 = 240, x4= 225, y4 = 0;
			BufferedImage img2 = new BufferedImage(w4, h4, BufferedImage.TYPE_INT_ARGB);
			img2.getGraphics().drawImage(bi, 0, 0, w4, h4, x4, y4, x4 + w4, y4 + h4, null);
			ImageIO.write(img2, "png", new File("C:\\SS\\img2.png"));
			progressBar.setString("19% - Splitting grid into individual squares");
			progressBar.setValue(19);
			progressBar.update(progressBar.getGraphics());
			x3 = x3 + 455;
			BufferedImage img3 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img3.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img3, "png", new File("C:\\SS\\img3.png"));
			progressBar.setString("20% - Splitting grid into individual squares");
			progressBar.setValue(20);
			progressBar.update(progressBar.getGraphics());
			x3 = x3 + 230;
			BufferedImage img4 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img4.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img4, "png", new File("C:\\SS\\img4.png"));
			progressBar.setString("21% - Splitting grid into individual squares");
			progressBar.setValue(21);
			progressBar.update(progressBar.getGraphics());
			x3 = x3 + 230;
			BufferedImage img5 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img5.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img5, "png", new File("C:\\SS\\img5.png"));
			x3 = x3 + 230;
			BufferedImage img6 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img6.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img6, "png", new File("C:\\SS\\img6.png"));
			y3 = y3 + 235;
			x3 = 0;
			BufferedImage img7 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img7.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img7, "png", new File("C:\\SS\\img7.png"));
			x3 = x3 + 230;
			BufferedImage img8 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img8.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img8, "png", new File("C:\\SS\\img8.png"));
			x3 = x3 + 230;
			BufferedImage img9 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img9.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img9, "png", new File("C:\\SS\\img9.png"));
			x3 = x3 + 230;
			BufferedImage img10 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img10.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img10, "png", new File("C:\\SS\\img10.png"));
			x3 = x3 + 230;
			BufferedImage img11 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img11.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img11, "png", new File("C:\\SS\\img11.png"));
			x3 = x3 + 230;
			BufferedImage img12 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img12.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img12, "png", new File("C:\\SS\\img12.png"));
			x3 = 0;
			y3 = y3 + 233;
			BufferedImage img13 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img13.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img13, "png", new File("C:\\SS\\img13.png"));
			x3 = x3 + 230;
			BufferedImage img14 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img14.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img14, "png", new File("C:\\SS\\img14.png"));
			x3 = x3 + 230;
			BufferedImage img15 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img15.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img15, "png", new File("C:\\SS\\img15.png"));
			x3 = x3 + 230;
			BufferedImage img16 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img16.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img16, "png", new File("C:\\SS\\img16.png"));
			x3 = x3 + 230;
			BufferedImage img17 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img17.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img17, "png", new File("C:\\SS\\img17.png"));
			x3 = x3 + 230;
			BufferedImage img18 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img18.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img18, "png", new File("C:\\SS\\img18.png"));
			x3 = 0;
			y3 = y3 + 230;
			BufferedImage img19 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img19.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img19, "png", new File("C:\\SS\\img19.png"));
			x3 = x3 + 230;
			BufferedImage img20 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img20.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img20, "png", new File("C:\\SS\\img20.png"));
			x3 = x3 + 230;
			BufferedImage img21 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img21.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img21, "png", new File("C:\\SS\\img21.png"));
			x3 = x3 + 230;
			BufferedImage img22 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img22.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img22, "png", new File("C:\\SS\\img22.png"));
			x3 = x3 + 230;
			BufferedImage img23 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img23.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img23, "png", new File("C:\\SS\\img23.png"));
			x3 = x3 + 230;
			BufferedImage img24 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img24.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img24, "png", new File("C:\\SS\\img24.png"));
			x3 = 0;
			y3 = y3 + 230;
			BufferedImage img25 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img25.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img25, "png", new File("C:\\SS\\img25.png"));
			x3 = x3 + 230;
			BufferedImage img26 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img26.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img26, "png", new File("C:\\SS\\img26.png"));
			x3 = x3 + 230;
			BufferedImage img27 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img27.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img27, "png", new File("C:\\SS\\img27.png"));
			x3 = x3 + 230;
			BufferedImage img28 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img28.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img28, "png", new File("C:\\SS\\img28.png"));
			x3 = x3 + 230;
			BufferedImage img29 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img29.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img29, "png", new File("C:\\SS\\img29.png"));
			x3 = x3 + 230;
			BufferedImage img30 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img30.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img30, "png", new File("C:\\SS\\img30.png"));
			x3 = 0;
			y3 = y3 + 230;
			BufferedImage img31 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img31.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img31, "png", new File("C:\\SS\\img31.png"));
			x3 = x3 + 230;
			BufferedImage img32 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img32.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img32, "png", new File("C:\\SS\\img32.png"));
			x3 = x3 + 230;
			BufferedImage img33 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img33.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img33, "png", new File("C:\\SS\\img33.png"));
			x3 = x3 + 230;
			BufferedImage img34 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img34.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img34, "png", new File("C:\\SS\\img34.png"));
			x3 = x3 + 230;
			BufferedImage img35 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img35.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img35, "png", new File("C:\\SS\\img35.png"));
			x3 = x3 + 230;
			BufferedImage img36 = new BufferedImage(w3, h3, BufferedImage.TYPE_INT_ARGB);
			img36.getGraphics().drawImage(bi, 0, 0, w3, h3, x3, y3, x3 + w3, y3 + h3, null);
			ImageIO.write(img36, "png", new File("C:\\SS\\img36.png"));
			progressBar.setString("30% - Grid split into individual squares");
			progressBar.setValue(30);
			progressBar.update(progressBar.getGraphics());
			//Time to Hash Images
			int x01 = 40, y01 = 85, w01 = 100, h01 = 95;
			BufferedImage hash1 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash1.getGraphics().drawImage(img1, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash1, "png", new File("C:\\SS\\hash1.png"));
			x01 = x01 + 7;
			BufferedImage hash2 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash2.getGraphics().drawImage(img2, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash2, "png", new File("C:\\SS\\hash2.png"));
			x01 = x01 + 5;
			x01= 47;
			x01 = x01+ 5;
			BufferedImage hash3 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash3.getGraphics().drawImage(img3, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash3, "png", new File("C:\\SS\\hash3.png"));
			x01 = x01+ 2;
			BufferedImage hash4 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash4.getGraphics().drawImage(img4, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash4, "png", new File("C:\\SS\\hash4.png"));
			x01 = 47;
			x01 = x01 + 10;
			BufferedImage hash5 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash5.getGraphics().drawImage(img5, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash5, "png", new File("C:\\SS\\hash5.png"));
			BufferedImage hash6 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			x01 = x01 + 1;
			hash6.getGraphics().drawImage(img6, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash6, "png", new File("C:\\SS\\hash6.png"));
			x01 = 40;
			y01 = y01 -3;
			BufferedImage hash7 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash7.getGraphics().drawImage(img7, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash7, "png", new File("C:\\SS\\hash7.png"));
			x01 = x01 + 7;
			BufferedImage hash8 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash8.getGraphics().drawImage(img8, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash8, "png", new File("C:\\SS\\hash8.png"));
			x01 = x01 + 5;
			x01= 47;
			BufferedImage hash9 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash9.getGraphics().drawImage(img9, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash9, "png", new File("C:\\SS\\hash9.png"));
			x01 = x01+ 2;
			BufferedImage hash10 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash10.getGraphics().drawImage(img10, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash10, "png", new File("C:\\SS\\hash10.png"));
			x01 = 47;
			x01 = x01 + 6;
			BufferedImage hash11 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash11.getGraphics().drawImage(img11, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash11, "png", new File("C:\\SS\\hash11.png"));
			x01 = x01 + 1;
			BufferedImage hash12 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash12.getGraphics().drawImage(img12, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash12, "png", new File("C:\\SS\\hash12.png"));
			x01 = 41;
			y01 = y01 +1;
			BufferedImage hash13 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash13.getGraphics().drawImage(img13, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash13, "png", new File("C:\\SS\\hash13.png"));
			x01 = 42;
			x01 = x01 + 1;
			BufferedImage hash14 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash14.getGraphics().drawImage(img14, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash14, "png", new File("C:\\SS\\hash14.png"));
			x01= 47;
			BufferedImage hash15 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash15.getGraphics().drawImage(img15, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash15, "png", new File("C:\\SS\\hash15.png"));
			x01 = x01+ 5;
			x01 = 47;
			BufferedImage hash16 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash16.getGraphics().drawImage(img16, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash16, "png", new File("C:\\SS\\hash16.png"));
			x01 = 45;
			x01 = x01 + 8;
			BufferedImage hash17 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash17.getGraphics().drawImage(img17, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash17, "png", new File("C:\\SS\\hash17.png"));
			x01 = 47;
			x01 = x01 + 11;
			BufferedImage hash18 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash18.getGraphics().drawImage(img18, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash18, "png", new File("C:\\SS\\hash18.png"));
			x01 = 40;
			
			BufferedImage hash19 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash19.getGraphics().drawImage(img19, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash19, "png", new File("C:\\SS\\hash19.png"));
			y01 = y01 +1;
			x01 = x01 +7;
			BufferedImage hash20 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash20.getGraphics().drawImage(img20, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash20, "png", new File("C:\\SS\\hash20.png"));
			x01 = x01 +4;
			BufferedImage hash21 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash21.getGraphics().drawImage(img21, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash21, "png", new File("C:\\SS\\hash21.png"));
			x01 = x01 -3;
			BufferedImage hash22 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash22.getGraphics().drawImage(img22, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash22, "png", new File("C:\\SS\\hash22.png"));
			x01 = x01 + 3;
			BufferedImage hash23 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash23.getGraphics().drawImage(img23, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash23, "png", new File("C:\\SS\\hash23.png"));
			x01 = x01 + 3;
			BufferedImage hash24 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash24.getGraphics().drawImage(img24, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash24, "png", new File("C:\\SS\\hash24.png"));
			y01= y01+4;
			x01 = 45;
			BufferedImage hash25 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash25.getGraphics().drawImage(img25, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash25, "png", new File("C:\\SS\\hash25.png"));
			x01 = x01 - 2;
			BufferedImage hash26 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash26.getGraphics().drawImage(img26, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash26, "png", new File("C:\\SS\\hash26.png"));
			x01 = x01 +5;
			BufferedImage hash27 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash27.getGraphics().drawImage(img27, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash27, "png", new File("C:\\SS\\hash27.png"));
			x01 = x01 -1;
			BufferedImage hash28 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash28.getGraphics().drawImage(img28, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash28, "png", new File("C:\\SS\\hash28.png"));
			x01 = x01 + 10;
			BufferedImage hash29 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash29.getGraphics().drawImage(img29, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash29, "png", new File("C:\\SS\\hash29.png"));
			x01 = x01 - 10;
			x01 = x01 + 10;
			y01= y01 +1 ;
			BufferedImage hash30 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash30.getGraphics().drawImage(img30, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash30, "png", new File("C:\\SS\\hash30.png"));
			y01 = y01 + 2;
			x01 = x01 - 5;
			BufferedImage hash31 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash31.getGraphics().drawImage(img31, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash31, "png", new File("C:\\SS\\hash31.png"));
			
			BufferedImage hash32 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash32.getGraphics().drawImage(img32, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash32, "png", new File("C:\\SS\\hash32.png"));
			
			BufferedImage hash33 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash33.getGraphics().drawImage(img33, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash33, "png", new File("C:\\SS\\hash33.png"));
			
			BufferedImage hash34 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash34.getGraphics().drawImage(img34, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash34, "png", new File("C:\\SS\\hash34.png"));
			x01 = x01 +10;
			BufferedImage hash35 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash35.getGraphics().drawImage(img35, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash35, "png", new File("C:\\SS\\hash35.png"));
			x01 = x01 - 10;
			x01 = x01 + 10;
			BufferedImage hash36 = new BufferedImage(w01, h01, BufferedImage.TYPE_INT_ARGB);
			hash36.getGraphics().drawImage(img36, 0, 0, w01, h01, x01, y01, x01 + w01, y01 + h01, null);
			ImageIO.write(hash36, "png", new File("C:\\SS\\hash36.png"));
			
			imageCheck(1);
			
//			ImagePHash me = new ImagePHash();
//			ByteArrayOutputStream baos = new ByteArrayOutputStream();
//			ImageIO.write(hash8, "png", baos);
//			InputStream is = new ByteArrayInputStream(baos.toByteArray());
//			String p = me.getHash(is);
//			System.out.println(p);
//			ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
//			ImageIO.write(hash13, "png", baos2);
//			InputStream is2 = new ByteArrayInputStream(baos2.toByteArray());
//			String p2 = me.getHash(is2);
//			System.out.println(p2);
//			System.out.println(me.distance(p, p2));
//			int ff = 49 - me.distance(p, p2);
//			float Percentage;
//			Percentage = (float) ((ff*100)/49);
//			int similarityscore = 14 /49;
//			System.out.println("% " + Percentage + " Similarity rating");
//			
			
			
					
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	
		
	}
	});
	
	public static void SetVirtualInput(){
		if(lastChar =="SPACE"){
			for (int i = 0; i < matches.matchedImages.size(); i++) {
				SpaceList.add(matches.matchedImages.get(i));
				
			 }
			 matches.matchedImages.clear();
		}
		
		if(lastChar =="A"){
			for (int i = 0; i < matches.matchedImages.size(); i++) {
				AList.add(matches.matchedImages.get(i));
				
			 }
			 matches.matchedImages.clear();
		}
		if(lastChar == "AF"){
			for (int i = 0; i < matches.matchedImages.size(); i++) {
				AFList.add(matches.matchedImages.get(i));
				
			 }
			 matches.matchedImages.clear();
		}
		if(lastChar == "S"){
			 firstS = matches.getAverageColour(matches.FullImages.indexOf(matches.matchedImages.get(0)) + 1);
			 for (int i = 0; i < matches.matchedImages.size(); i++) {
					SList.add(matches.matchedImages.get(i));
					//matches.matchedImages.clear();
				}
			 matches.matchedImages.clear();
			 
		 }
		if(lastChar == "SF"){
			 firstS = matches.getAverageColour(matches.FullImages.indexOf(matches.matchedImages.get(0)) + 1);
			 for (int i = 0; i < matches.matchedImages.size(); i++) {
					SFList.add(matches.matchedImages.get(i));
					//matches.matchedImages.clear();
				}
			 matches.matchedImages.clear();
			 
		 }
		if(lastChar == "E"){
			 for (int i = 0; i < matches.matchedImages.size(); i++) {
					EList.add(matches.matchedImages.get(i));
					//matches.matchedImages.clear();
				}
			 matches.matchedImages.clear();
		 }
		if(lastChar == "EF"){
			 for (int i = 0; i < matches.matchedImages.size(); i++) {
					EFList.add(matches.matchedImages.get(i));
					//matches.matchedImages.clear();
				}
			 matches.matchedImages.clear();
		 }
		if(lastChar == "R"){
			 for (int i = 0; i < matches.matchedImages.size(); i++) {
					RList.add(matches.matchedImages.get(i));
					//matches.matchedImages.clear();
				}
			 matches.matchedImages.clear();
		 }
		if(lastChar == "RF"){
			 for (int i = 0; i < matches.matchedImages.size(); i++) {
					RFList.add(matches.matchedImages.get(i));
					//matches.matchedImages.clear();
				}
			 matches.matchedImages.clear();
		 }
		if(lastChar == "G"){
			 for (int i = 0; i < matches.matchedImages.size(); i++) {
					GList.add(matches.matchedImages.get(i));
					//matches.matchedImages.clear();
				}
			 matches.matchedImages.clear();
		 }
		if(lastChar == "GF"){
			 for (int i = 0; i < matches.matchedImages.size(); i++) {
					GFList.add(matches.matchedImages.get(i));
					//matches.matchedImages.clear();
				}
			 matches.matchedImages.clear();
		 }
		if(lastChar == "D"){
			 for (int i = 0; i < matches.matchedImages.size(); i++) {
					DList.add(matches.matchedImages.get(i));
					//matches.matchedImages.clear();
				}
			 matches.matchedImages.clear();
		 }
		if(lastChar == "DF"){
			 for (int i = 0; i < matches.matchedImages.size(); i++) {
					DFList.add(matches.matchedImages.get(i));
					//matches.matchedImages.clear();
				}
			 matches.matchedImages.clear();
		 }
		if(lastChar == "W"){
			 for (int i = 0; i < matches.matchedImages.size(); i++) {
					WList.add(matches.matchedImages.get(i));
					//matches.matchedImages.clear();
				}
			 matches.matchedImages.clear();
		 }
		if(lastChar == "WF"){
			 for (int i = 0; i < matches.matchedImages.size(); i++) {
					WFList.add(matches.matchedImages.get(i));
					//matches.matchedImages.clear();
				}
			 matches.matchedImages.clear();
		 }
		if(lastChar == "T"){
			 for (int i = 0; i < matches.matchedImages.size(); i++) {
					TList.add(matches.matchedImages.get(i));
					//matches.matchedImages.clear();
				}
			 matches.matchedImages.clear();
		 }
		 if(lastChar == "TF"){
			 for (int i = 0; i < matches.matchedImages.size(); i++) {
					TFList.add(matches.matchedImages.get(i));
					//matches.matchedImages.clear();
				}
			 matches.matchedImages.clear();
		 }
		 if(lastChar == "Q"){
			 for (int i = 0; i < matches.matchedImages.size(); i++) {
					QList.add(matches.matchedImages.get(i));
					//matches.matchedImages.clear();
				}
			 matches.matchedImages.clear();
		 }
		 if(lastChar == "QF"){
			 for (int i = 0; i < matches.matchedImages.size(); i++) {
					QFList.add(matches.matchedImages.get(i));
					//matches.matchedImages.clear();
				}
			 matches.matchedImages.clear();
		 }
		 if(lastChar == "B"){
			 for (int i = 0; i < matches.matchedImages.size(); i++) {
					BList.add(matches.matchedImages.get(i));
					//matches.matchedImages.clear();
				}
			 matches.matchedImages.clear();
		 }
		 if(lastChar == "BF"){
			 for (int i = 0; i < matches.matchedImages.size(); i++) {
					BFList.add(matches.matchedImages.get(i));
					//matches.matchedImages.clear();
				}
			 matches.matchedImages.clear();
		 }
		
		
	}
	
	//Checks the userInput and adds the matched images to the corresponding list
		public static void SetInput(String pokeChar){
			
			if(pokeChar.equals(" ")){
				ActiveKeys.add("Space");
				 firstSpace = matches.getAverageColour(matches.FullImages.indexOf(matches.matchedImages.get(0)) + 1);
				 for (int i = 0; i < matches.matchedImages.size(); i++) {
					SpaceList.add(matches.matchedImages.get(i));
					
				 }
				 matches.matchedImages.clear();
			 }
			
			if(pokeChar.equals("A")){
				ActiveKeys.add("A");
				 firstA = matches.getAverageColour(matches.FullImages.indexOf(matches.matchedImages.get(0)) + 1);
				 for (int i = 0; i < matches.matchedImages.size(); i++) {
					AList.add(matches.matchedImages.get(i));
					
				 }
				 matches.matchedImages.clear();
			 }
			 
			 if(pokeChar.equals("AF")){
				 ActiveKeys.add("AF");
				 firstAF = matches.getAverageColour(matches.FullImages.indexOf(matches.matchedImages.get(0)) + 1);;
				 for (int i = 0; i < matches.matchedImages.size(); i++) {
					 
					AFList.add(matches.matchedImages.get(i));
					//matches.matchedImages.clear();
				 }
				 matches.matchedImages.clear();
			 }
				
			 if(pokeChar.equals("S") ){
				 ActiveKeys.add("S");
				 firstS = matches.getAverageColour(matches.FullImages.indexOf(matches.matchedImages.get(0)) + 1);
				 for (int i = 0; i < matches.matchedImages.size(); i++) {
						SList.add(matches.matchedImages.get(i));
						//matches.matchedImages.clear();
					}
				 matches.matchedImages.clear();
				 }
			 
			 
			 if(pokeChar.equals("SF")){
				 ActiveKeys.add("SF");
				 firstSF = matches.getAverageColour(matches.FullImages.indexOf(matches.matchedImages.get(0)) + 1);
				 for (int i = 0; i < matches.matchedImages.size(); i++) {
						SFList.add(matches.matchedImages.get(i));
						//matches.matchedImages.clear();
					}
				 matches.matchedImages.clear();
				 }
			 
			 
			 
			 if(pokeChar.equals("E") ){
				 ActiveKeys.add("E");
				 firstE = matches.getAverageColour(matches.FullImages.indexOf(matches.matchedImages.get(0)) + 1);
				 for (int i = 0; i < matches.matchedImages.size(); i++) {
						EList.add(matches.matchedImages.get(i));
						//matches.matchedImages.clear();
					}
				 matches.matchedImages.clear();
				 }
			 
			 if(pokeChar.equals("EF")){
				 ActiveKeys.add("EF");
				 firstEF = matches.getAverageColour(matches.FullImages.indexOf(matches.matchedImages.get(0)) + 1);
				 for (int i = 0; i < matches.matchedImages.size(); i++) {
						EFList.add(matches.matchedImages.get(i));
						//matches.matchedImages.clear();
					}
				 matches.matchedImages.clear();
				 }
				
			 
			 if(pokeChar.equals("R")){
				 ActiveKeys.add("R");
				 firstR = matches.getAverageColour(matches.FullImages.indexOf(matches.matchedImages.get(0)) + 1);
				 for (int i = 0; i < matches.matchedImages.size(); i++) {
						RList.add(matches.matchedImages.get(i));
						//matches.matchedImages.clear();
					}
				 matches.matchedImages.clear();
			 }
			 
			 if(pokeChar.equals("RF")){
				 ActiveKeys.add("RF");
				 firstRF = matches.getAverageColour(matches.FullImages.indexOf(matches.matchedImages.get(0)) + 1);
				 for (int i = 0; i < matches.matchedImages.size(); i++) {
						RFList.add(matches.matchedImages.get(i));
						//matches.matchedImages.clear();
					}
				 matches.matchedImages.clear();
			 }
			 
			 
			 if(pokeChar.equals("G")){
				 ActiveKeys.add("G");
				 firstG = matches.getAverageColour(matches.FullImages.indexOf(matches.matchedImages.get(0)) + 1);
				 for (int i = 0; i < matches.matchedImages.size(); i++) {
						GList.add(matches.matchedImages.get(i));
						//matches.matchedImages.clear();
					}
				 matches.matchedImages.clear();
			 }
			 
			 
			 if(pokeChar.equals("GF")){
				 ActiveKeys.add("GF");
				 firstGF = matches.getAverageColour(matches.FullImages.indexOf(matches.matchedImages.get(0)) + 1);
				 for (int i = 0; i < matches.matchedImages.size(); i++) {
						GFList.add(matches.matchedImages.get(i));
						//matches.matchedImages.clear();
					}
				 matches.matchedImages.clear();
			 }
			 
			 
			 if(pokeChar.equals("D")){
				 ActiveKeys.add("D");
				 firstD = matches.getAverageColour(matches.FullImages.indexOf(matches.matchedImages.get(0)) + 1);
				 for (int i = 0; i < matches.matchedImages.size(); i++) {
						DList.add(matches.matchedImages.get(i));
						//matches.matchedImages.clear();
					}
				 matches.matchedImages.clear();
			 }
			 
			 
			 if(pokeChar.equals("DF")){
				 ActiveKeys.add("DF");
				 firstDF = matches.getAverageColour(matches.FullImages.indexOf(matches.matchedImages.get(0)) + 1);
				 for (int i = 0; i < matches.matchedImages.size(); i++) {
						DFList.add(matches.matchedImages.get(i));
						//matches.matchedImages.clear();
					}
				 matches.matchedImages.clear();
			 }
			 
			 if(pokeChar.equals("W")){
				 ActiveKeys.add("W");
				 firstW = matches.getAverageColour(matches.FullImages.indexOf(matches.matchedImages.get(0)) + 1);
				 for (int i = 0; i < matches.matchedImages.size(); i++) {
						WList.add(matches.matchedImages.get(i));
						//matches.matchedImages.clear();
					}
				 matches.matchedImages.clear();
			 }
			 
			 
			 if(pokeChar.equals("WF")){
				 ActiveKeys.add("WF");
				 firstWF = matches.getAverageColour(matches.FullImages.indexOf(matches.matchedImages.get(0)) + 1);
				 for (int i = 0; i < matches.matchedImages.size(); i++) {
						WFList.add(matches.matchedImages.get(i));
						//matches.matchedImages.clear();
					}
				 matches.matchedImages.clear();
			 }
			 if(pokeChar.equals("T")){
				 ActiveKeys.add("T");
				 firstT = matches.getAverageColour(matches.FullImages.indexOf(matches.matchedImages.get(0)) + 1);
				 for (int i = 0; i < matches.matchedImages.size(); i++) {
						TList.add(matches.matchedImages.get(i));
						//matches.matchedImages.clear();
					}
				 matches.matchedImages.clear();
			 }
			 
			 
			 
			 if(pokeChar.equals("TF")){
				 ActiveKeys.add("TF");
				 firstTF = matches.getAverageColour(matches.FullImages.indexOf(matches.matchedImages.get(0)) + 1);
				 for (int i = 0; i < matches.matchedImages.size(); i++) {
						TFList.add(matches.matchedImages.get(i));
						//matches.matchedImages.clear();
					}
				 matches.matchedImages.clear();
			 }
			
			 if(pokeChar.equals("Q")){
				 ActiveKeys.add("Q");
				 firstQ = matches.getAverageColour(matches.FullImages.indexOf(matches.matchedImages.get(0)) + 1);
				 for (int i = 0; i < matches.matchedImages.size(); i++) {
						QList.add(matches.matchedImages.get(i));
						//matches.matchedImages.clear();
					}
				 matches.matchedImages.clear();
			 }
			
			 
			 if(pokeChar.equals("QF")){
				 ActiveKeys.add("QF");
				 firstQF = matches.getAverageColour(matches.FullImages.indexOf(matches.matchedImages.get(0)) + 1);
				 for (int i = 0; i < matches.matchedImages.size(); i++) {
						QFList.add(matches.matchedImages.get(i));
						//matches.matchedImages.clear();
					}
				 matches.matchedImages.clear();
			 }
			
			
			 if(pokeChar.equals("B")){
				 ActiveKeys.add("B");
				 firstB = matches.getAverageColour(matches.FullImages.indexOf(matches.matchedImages.get(0)) + 1);
				 for (int i = 0; i < matches.matchedImages.size(); i++) {
						BList.add(matches.matchedImages.get(i));
						//matches.matchedImages.clear();
					}
				 matches.matchedImages.clear();
			 }
			 
			 if(pokeChar.equals("BF")){
				 ActiveKeys.add("BF");
				 firstBF = matches.getAverageColour(matches.FullImages.indexOf(matches.matchedImages.get(0)) + 1);
				 for (int i = 0; i < matches.matchedImages.size(); i++) {
						BFList.add(matches.matchedImages.get(i));
						//matches.matchedImages.clear();
					}
				 matches.matchedImages.clear();
			 }
			 
			 
			 
			 System.out.println("REMAINING IMAGES - " + matches.Images);
			 System.out.println(SpaceList);
			 System.out.println(AList);
			 System.out.println(SList);
			 System.out.println(EList);
			 System.out.println(RList);
			 System.out.println(GList);
			 System.out.println(DList);
			 System.out.println(WList);
			 System.out.println(TList);
			 System.out.println(QList);
			 
			 System.out.println(AFList);
			 System.out.println(SFList);
			 System.out.println(EFList);
			 System.out.println(RFList);
			 System.out.println(GFList);
			 System.out.println(DFList);
			 System.out.println(WFList);
			 System.out.println(TFList);
			 System.out.println(QFList);
			 
		}
		
		// adds filenames in order 1-36 to char list
		public static void AddToCharList(){
			
		 for (int i = 0; i < Paths.size(); i++) 
		 {
			 String fileName = Paths.get(i);
			 
			 if(SpaceList.contains(fileName) ){
				 System.out.print("SPACE");
				 finalCharList.add("SPACE");
				 //SpaceList.remove(AList.indexOf(fileName));
			 }
			
			 if(AList.contains(fileName) ){
				 System.out.print("A");
				 finalCharList.add("A");
				 //AList.remove(AList.indexOf(fileName));
			 }
			 if(AFList.contains(fileName)){
				 System.out.print("AF");
				 finalCharList.add("AF");
				 //AFList.remove(AList.indexOf(fileName));
			 }
			 if(SList.contains(fileName)){
				 System.out.print("S");
				 finalCharList.add("S");
				 //SList.remove(SList.indexOf(fileName));
			 }
			 if(SFList.contains(fileName)){
				 System.out.print("SF");
				 finalCharList.add("SF");
				// SFList.remove(SList.indexOf(fileName));
			 }
			 if(EList.contains(fileName)){
				 System.out.print("E");
				 finalCharList.add("E");
				 //EList.remove(EList.indexOf(fileName));
			 }
			 if(EFList.contains(fileName)){
				 System.out.print("EF");
				 finalCharList.add("EF");
				 //EFList.remove(EList.indexOf(fileName));
			 }
			 if(RList.contains(fileName)){
				 System.out.print("R");
				 finalCharList.add("R");
				 //RList.remove(RList.indexOf(fileName));
			 }
			 if(RFList.contains(fileName)){
				 System.out.print("RF");
				 finalCharList.add("RF");
				 //RFList.remove(RList.indexOf(fileName));
			 }
			 if(GList.contains(fileName)){
				 System.out.print("G");
				 finalCharList.add("G");
				// GList.remove(RList.indexOf(fileName));
			 }
			 if(GFList.contains(fileName)){
				 System.out.print("GF");
				 finalCharList.add("GF");
				 //GFList.remove(RList.indexOf(fileName));
			 }
			 if(DList.contains(fileName)){
				 System.out.print("D");
				 finalCharList.add("D");
				 //DList.remove(RList.indexOf(fileName));
			 }
			 if(DFList.contains(fileName)){
				 System.out.print("DF");
				 finalCharList.add("DF");
				//RList.remove(RList.indexOf(fileName));
			 }
			 if(WList.contains(fileName)){
				 System.out.print("W");
				 finalCharList.add("W");
				 //RList.remove(RList.indexOf(fileName));
			 }
			 if(WFList.contains(fileName)){
				 System.out.print("WF");
				 finalCharList.add("WF");
				 //RList.remove(RList.indexOf(fileName));
			 }
			if(TList.contains(fileName)){
				 System.out.print("T");
				 finalCharList.add("T");
				 //RList.remove(RList.indexOf(fileName));
			 }
			 if(TFList.contains(fileName)){
				 System.out.print("TF");
				 finalCharList.add("TF");
				 //RList.remove(RList.indexOf(fileName));
			 }
			 if(QList.contains(fileName)){
				 System.out.print("Q");
				 finalCharList.add("Q");
				 //RList.remove(RList.indexOf(fileName));
			 }
			 if(QFList.contains(fileName)){
				 System.out.print("QF");
				 finalCharList.add("QF");
				 //RList.remove(RList.indexOf(fileName));
			 }
			 if(BList.contains(fileName)){
				 System.out.print("B");
				 finalCharList.add("B");
				 //RList.remove(RList.indexOf(fileName));
			 }
			if(BFList.contains(fileName)){
				 System.out.print("BF");
				 finalCharList.add("BF");
				 //RList.remove(RList.indexOf(fileName));
			 }
			 
			 
			 
		 }
	}
		
		
		//Types the characters in the list to the board
		public static void RobotType(){

			Robot robot;
			try {
				robot = new Robot();
				for (int k = 0; k < finalCharList.size(); k++) {
					
					 if(finalCharList.get(k) == "SPACE"){
						 robot.keyPress(keyInput[11]);
						 robot.delay(10);
					 }
					 else if(finalCharList.get(k) == "A"){
						 robot.keyPress(keyInput[0]);
						 robot.delay(10);
					 }
					 else if(finalCharList.get(k) == "AF"){
						 robot.keyPress(keyInput[0]);
						 robot.delay(10);
						 robot.keyPress(keyInput[10]);
						 robot.delay(10);
					 }
					 else if(finalCharList.get(k) == "S"){
						 robot.keyPress(keyInput[1]);
						 robot.delay(10);
					 }
					 else if(finalCharList.get(k) == "SF"){
						 robot.keyPress(keyInput[1]);
						 robot.delay(10);
						 robot.keyPress(keyInput[10]);
						 robot.delay(10);
					 }
					 
					 else if(finalCharList.get(k) == "E"){
						 robot.keyPress(keyInput[2]);
						 robot.delay(10);
					 }
					 
					 else if(finalCharList.get(k) == "EF"){
						 robot.keyPress(keyInput[2]);
						 robot.delay(10);
						 robot.keyPress(keyInput[10]);
						 robot.delay(10);
					 }
					 
					 else if(finalCharList.get(k) == "R"){
						 robot.keyPress(keyInput[3]);
						 robot.delay(10);
					 }
					 else if(finalCharList.get(k) == "RF"){
						 robot.keyPress(keyInput[3]);
						 robot.delay(10);
						 robot.keyPress(keyInput[10]);
						 robot.delay(10);
					 }
					 else if(finalCharList.get(k) == "G"){
						 robot.keyPress(keyInput[4]);
						 robot.delay(10);
					 }
					 else if(finalCharList.get(k) == "GF"){
						 robot.keyPress(keyInput[4]);
						 robot.delay(10);
						 robot.keyPress(keyInput[10]);
						 robot.delay(10);
					 }
					 else if(finalCharList.get(k) == "D"){
						 robot.keyPress(keyInput[5]);
						 robot.delay(10);
					 }
					 else if(finalCharList.get(k) == "DF"){
						 robot.keyPress(keyInput[5]);
						 robot.delay(10);
						 robot.keyPress(keyInput[10]);
						 robot.delay(10);
					 }
					 else if(finalCharList.get(k) == "W"){
						 robot.keyPress(keyInput[6]);
						 robot.delay(10);
					 }
					 else if(finalCharList.get(k) == "WF"){
						 robot.keyPress(keyInput[6]);
						 robot.delay(10);
						 robot.keyPress(keyInput[10]);
						 robot.delay(10);
					 }
					 else if(finalCharList.get(k) == "T"){
						 robot.keyPress(keyInput[7]);
						 robot.delay(10);
					 }
					 else if(finalCharList.get(k) == "TF"){
						 robot.keyPress(keyInput[7]);
						 robot.delay(10);
						 robot.keyPress(keyInput[10]);
						 robot.delay(10);
					 }
					 else if(finalCharList.get(k) == "Q"){
						 robot.keyPress(keyInput[8]);
						 robot.delay(10);
					 }
					 else if(finalCharList.get(k) == "QF"){
						 robot.keyPress(keyInput[8]);
						 robot.delay(10);
						 robot.keyPress(keyInput[10]);
						 robot.delay(10);
					 }
					 else if(finalCharList.get(k) == "B"){
						 robot.keyPress(keyInput[9]);
						 robot.delay(10);
					 }
					 else if(finalCharList.get(k) == "BF"){
						 robot.keyPress(keyInput[9]);
						 robot.delay(10);
						 robot.keyPress(keyInput[10]);
						 robot.delay(10);
					 }
			
				 } //end of for loop
				 } catch (AWTException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
				
		}
		}
			
			
		public static void Reset(){
			 finalCharList.clear();
			 matches.matchedImages.clear();
			 matches.nonMatchedImages.clear();
			 SpaceList.clear();
			 AList.clear();
			 SList.clear();
			 EList.clear();
			 RList.clear();
			 GList.clear();
			 DList.clear();
			 WList.clear();
			 TList.clear();
			 QList.clear();
			 BList.clear();
			 AFList.clear();
			 SFList.clear();
			 EFList.clear();
			 RFList.clear();
			 GFList.clear();
			 DFList.clear();
			 WFList.clear();
			 TFList.clear();
			 QFList.clear();
			 BFList.clear();
			 matches.FirstRun = true;
			 firstTimeThrough = false;
		}
		
		
		JButton btnOk = new JButton(new AbstractAction("OK"){
			 @Override
			 
			 // A S E R
		        public void actionPerformed( ActionEvent e ) {

				 textField.requestFocus();
				 String pokeChar = textField.getText().toUpperCase();
				
				 SetInput(pokeChar); //Checks the userInput and adds the matched images to the corresponding list
				 
				
				 if(matches.Images.size() > 0){ //If the image list has unmatched images in it
					 
					 if(firstTimeThrough){
						 String part1 =  matches.Images.get(0).substring(4);
						 String[] parts = part1.split(".png");
						 String finalString = parts[0];
						 int finalInt = Integer.parseInt(finalString);
						 imageCheck(finalInt); 
					 }
					 
					 
				 
				 }
				 else //ELSE END
				 {
					 //Input data into ShuffleMove
					 System.out.println("end");
					 btnOk.setEnabled(false);
					 
					 System.out.println("Charlist number" + finalCharList.size());
					AddToCharList();
					 
					 for (int j = 0; j < finalCharList.size(); j++) 
					 {
						 System.out.print(finalCharList.get(j));
					 }
					 
					 try {
						Thread.sleep(5000);
					} catch (InterruptedException e2) {
						e2.printStackTrace();
					}
					 
					 RobotType();
					 Reset();
						
				 }
				 textField.setText("");
				 textField.requestFocus();
			 }
			 
		});
		
		
		public void imageCheck(int n){
			try{
			File d = new File("C:\\SS\\img" + n + ".png");
			ImageIcon icon1 = new ImageIcon("C:\\SS\\img" + n + ".png"); 
			Image dip1 = ImageIO.read(d);
			lblNewLabel.setText("");
			lblNewLabel.setIcon(new ImageIcon(dip1.getScaledInstance(411, 302, 411)));
			lblNewLabel.update(lblNewLabel.getGraphics());
			textField.setEnabled(true);
			lblEnterHotkeyFor.setEnabled(true);
			btnOk.setEnabled(true);
			 
			runMatcher(n);
			}
			catch(Exception e){
				//System.out.println();
				e.printStackTrace();
			}
			
		}
		
		public void runMatcher(int imageToCheck){
			//matches matcher = new matches();
			if (firstTimeThrough || checkPokemon){
				matches.getMatches(imageToCheck);
				checkPokemon = false;
			}
			else{
				
				//System.out.println(numberOfChars);
				//System.out.println("Active keys - " + ActiveKeys.size());
				
				for (int i = 0; i < ActiveKeys.size() ; i++) {
					
					//System.out.println(i);
					if(ActiveKeys.get(i).equals("SPACE")){
						if(!firstSpace.isEmpty()){
							lastChar = "SPACE";
							
							matches.getFurtherMatches(firstSpace);
						}
					}
					if(ActiveKeys.get(i).equals("A")){
						if(!firstA.isEmpty()){
							lastChar = "A";
							
							matches.getFurtherMatches(firstA);
						}
					}
					if(ActiveKeys.get(i).equals("AF")){
						if(!firstAF.isEmpty()){
							lastChar = "AF";
							
							matches.getFurtherMatches(firstAF);
						}
					}
					if(ActiveKeys.get(i).equals("S")){
						lastChar = "S";
						if(!firstS.isEmpty()){
							
							matches.getFurtherMatches(firstS);
						}
					}
					if(ActiveKeys.get(i).equals("SF")){
						lastChar = "SF";
						if(!firstSF.isEmpty()){
							
							matches.getFurtherMatches(firstSF);
						}
					}
					if(ActiveKeys.get(i).equals("E")){
						if(!firstE.isEmpty()){
							lastChar = "E";
							
							matches.getFurtherMatches(firstE);
						}
					}
					if(ActiveKeys.get(i).equals("EF")){
						if(!firstEF.isEmpty()){
							lastChar = "EF";
							
							matches.getFurtherMatches(firstEF);
						}
					}
					if(ActiveKeys.get(i).equals("R")){
						lastChar = "R";
						if(!firstR.isEmpty()){
							
							matches.getFurtherMatches(firstR);
						}
					}
					if(ActiveKeys.get(i).equals("RF")){
						lastChar = "RF";
						if(!firstRF.isEmpty()){
							
							matches.getFurtherMatches(firstRF);
							
						}
					}
					if(ActiveKeys.get(i).equals("G")){
						lastChar = "G";
						if(!firstG.isEmpty()){
							
							matches.getFurtherMatches(firstG);
							
						}
					}
					if(ActiveKeys.get(i).equals("GF")){
						lastChar = "GF";
						if(!firstGF.isEmpty()){
							System.out.println(firstGF);
							matches.getFurtherMatches(firstGF);
							
						}
					}
					if(ActiveKeys.get(i).equals("D")){
						lastChar = "D";
						if(!firstD.isEmpty()){
							matches.getFurtherMatches(firstD);
							
						}
					}
					if(ActiveKeys.get(i).equals("DF")){
						lastChar = "DF";
						if(!firstDF.isEmpty()){
							matches.getFurtherMatches(firstDF);
							
						}
					}
					if(ActiveKeys.get(i).equals("W")){
						lastChar = "W";
						if(!firstW.isEmpty()){
							matches.getFurtherMatches(firstW);
							
						}
					}
					if(ActiveKeys.get(i).equals("WF")){
						lastChar = "WF";
						if(!firstWF.isEmpty()){
							matches.getFurtherMatches(firstWF);
						}
					}
					
					if(ActiveKeys.get(i).equals("T")){
						lastChar = "T";
						if(!firstT.isEmpty()){
							matches.getFurtherMatches(firstT);
							
						}
					}
					
					if(ActiveKeys.get(i).equals("TF")){
						lastChar = "TF";
						if(!firstTF.isEmpty()){
							matches.getFurtherMatches(firstTF);
							
						}
					}
					
					if(ActiveKeys.get(i).equals("Q")){
						lastChar = "Q";
						if(!firstQ.isEmpty()){
							matches.getFurtherMatches(firstQ);
							
						}
					}
					if(ActiveKeys.get(i).equals("QF")){
						lastChar = "QF";
						if(!firstQF.isEmpty()){
							matches.getFurtherMatches(firstQF);
							
						}
					}
					SetVirtualInput();
					lastChar = "";
					
					numberOfChars++;
					System.out.println("NUMBER OF CHARS - " + numberOfChars);
					System.out.println("ACTIVE KEYS - " + ActiveKeys.size());
					
				}
				
				
				
			if(matches.Images.size() > 0){
				 String part1 =  matches.Images.get(0).substring(4);
				 String[] parts = part1.split(".png");
				 String finalString = parts[0];
				 int finalInt = Integer.parseInt(finalString);
				 System.out.println("IMAGE TO CHECK - " + finalInt);
				 checkPokemon = true;
				 imageCheck(finalInt);
			 }
			 
			else{
				
				// END
				
				AddToCharList();
				System.out.print(finalCharList.size());

				 try {
					Thread.sleep(5000);
				} catch (InterruptedException e2) {
					e2.printStackTrace();
				}
				 
				 RobotType();
				 Reset();
					
				}
			}
			System.out.println(matches.Images);
			
			
			for (int i = 0; i < matches.matchedImages.size(); i++)
			{
				System.out.print(matches.matchedImages.get(i)  + " ");
			}
			
			System.out.println();
			
			for (int i = 0; i < matches.nonMatchedImages.size(); i++) 
			{
				System.out.print(matches.nonMatchedImages.get(i) + " ");
			}

			System.out.println();
			
//			for (int i = 0; i < matches.Images.size(); i++) 
//			{
//				System.out.print(" Images "  + matches.Images.get(i) + " ");
//				System.out.println("ImageCount " + matches.Images.size());
//			}
			
		}
		
		public boolean screenshotPhone(){
			File file = new File("C:\\SS\\screen.png");
			if(file.exists()){
				return true;
			}else{
				return false;
			}
		}
		
		private BufferedImage cropImage(BufferedImage src, Rectangle rect) {
		      BufferedImage dest = src.getSubimage(0, 0, rect.width, rect.height);
		      return dest; 
		   }
		 
		public static void main(String[] args) {
			ShuffleMoveFiller x = new ShuffleMoveFiller();
		}


		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			
		}
	}