import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import java.awt.Color;
import javax.swing.ImageIcon;
import java.awt.Panel;
import javax.swing.JTextField;

public class GUI {

	JPanel panel;
	JButton button;
	JFrame frame;
	private JTextField textField;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame=new JFrame();   
		panel=new JPanel();
		button=new JButton("Fill ShuffleMove");
		button.setBounds(222, 427, 132, 23);
		
		frame.setTitle("ShuffleMoveFiller");  //Sets the title of the applications window and also its size
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(600,500);
		frame.setVisible(true);
		frame.getContentPane().add(panel,BorderLayout.CENTER);
		panel.setLayout(null);
		panel.add(button);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setBounds(117, 387, 332, 14);
		panel.add(progressBar);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(76, 27, 411, 302);
		panel.add(lblNewLabel);
		
		JLabel lblCurrentGrid = new JLabel("Current Grid:");
		lblCurrentGrid.setBounds(253, 2, 153, 14);
		panel.add(lblCurrentGrid);
		
		JLabel lblEnterHotkeyFor = new JLabel("Enter Hotkey for this pokemon:");
		lblEnterHotkeyFor.setEnabled(false);
		lblEnterHotkeyFor.setBounds(222, 340, 188, 14);
		panel.add(lblEnterHotkeyFor);
		
		textField = new JTextField();
		textField.setEnabled(false);
		textField.setBounds(242, 356, 86, 20);
		panel.add(textField);
		textField.setColumns(10);
		
		JButton btnOk = new JButton("OK");
		btnOk.setEnabled(false);
		btnOk.setBounds(338, 355, 89, 23);
		panel.add(btnOk);
	}
}
