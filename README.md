## Puzzle Game sprite recognition/matching tool

### What it does

* Takes a photo of the game screen on a connected mobile phone using ADB. 
* Splits the game screen into a grid programmatically to split up each individual sprite.
* Gets the RGB values of each individual pixel of each sprite.
* Uses the RGB value information to identify the sprite.
* Inputs these identified sprite combinations into another tool which is used to calculate the best move the user should perform to earn maximum points on the game.