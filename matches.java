import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import java.awt.Color;
import java.util.List;

/*
 *  Aaron Cunliffe
*/

public class matches {
	public static List<String> Images = new ArrayList<String>();

	 
	 public static final List<String> FullImages = new ArrayList<String>() {{
			    	for (int i = 1; i <= 36; i++) {
						add("hash" + i + ".png");
						
					}
			    }};
	 
	
	 public static List<String> matchedImages = new ArrayList<String>();
	
	 public static List<String> nonMatchedImages = new ArrayList<String>();
	 
	 public static boolean FirstRun = true;
	 //ALL IMAGES
	

	 public static void fillImages(){
		 for (int i = 1; i <= 36; i++) {
			Images.add("hash" + i + ".png");
		}
	 }
	 
	 public static void getFurtherMatches(String average){
		 
		 if(FirstRun){
				fillImages();
				FirstRun = false;
			}
			
			try {
				//Image loadedImage = ImageIO.read(new File("A:\\Drive Locations\\Desktop\\ImageRecognition\\img1.jpg"));
				//int x = 30, y = 947, w = 1380, h = 1395;
			int x = 0, y = 0, w = 75, h = 75;
			int matchcount = 0;
			int nomatchcount = 0;
			
			
			BufferedImage image2 = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
			
			for (int i = 1; i <= 36; i++) {
				String pathString = "C:\\SS\\hash" + i  + ".png";
				image2 = ImageIO.read(new File(pathString));
				
				String[] parts = average.split(":");
				String R = parts[0];
				String G = parts[1];
				String B = parts[2];
				
				int image1R = Integer.parseInt(R);
				int image1G = Integer.parseInt(G);
				int image1B = Integer.parseInt(B);
				
				Color image2Color = (averageColor(image2, x, y, w, h));
				int image2R = image2Color.getRed();
				int image2G = image2Color.getGreen();
				int image2B = image2Color.getBlue();
				
				int differenceR = Math.abs(image1R - image2R);
				int differenceG = Math.abs(image1G - image2G);
				int differenceB = Math.abs(image1B - image2B);
				int tolerance = 10;
				
//		        System.out.println("Image 1 - R:" + image1R + " G:" + image1G + " B:" + image1B);
//		        System.out.println("Image " + i +"- R:" + image2R + " G:" + image2G + " B:" + image2B);
//		        System.out.println("");
//		        
//		        System.out.println("Difference - R:" + differenceR + " G:" + differenceG + " B:" + differenceB );
//		        System.out.println("");
				
		        int lowerBoundR = image1R - tolerance;
		        int upperBoundR = image1R + tolerance;
		        
		        int lowerBoundG = image1G - tolerance;
		        int upperBoundG = image1G + tolerance;
		        
		        int lowerBoundB = image1B - tolerance;
		        int upperBoundB = image1B + tolerance;
		        
		    
		        if ((lowerBoundR <= image2R &&  image2R <= upperBoundR) && (lowerBoundG <= image2G &&  image2G <= upperBoundG) && (lowerBoundB <= image2B &&  image2B <= upperBoundB))  
		        {
//		        	System.out.println("Match");
		        	matchedImages.add("hash" + i  + ".png");
		        	
		        	matchcount++;
		        }
		        else{
//		        	System.out.println("No match");
		        	nonMatchedImages.add("hash" + i  + ".png");
		        	nomatchcount++;
		        }
			}
			
			for (int i = 0; i < matchedImages.size(); i++) {
				Images.remove(matchedImages.get(i));
			}
			
			
		    System.out.println("Matches = " + matchcount );
		    System.out.println("No Matches = " + nomatchcount );
			
	       //matchedImages.clear();
	      // nonMatchedImages.clear();
		} catch (IOException e) {
			e.printStackTrace();
		}
		}
		
	 
//	 public static boolean isMatch(int imagetoCheck, String colorToCheck){
//		 try {
//			 if(FirstRun){
//					fillImages();
//					FirstRun = false;
//				}
//			 System.out.println("COLOR TO CHECK : " + colorToCheck);
//				//Image loadedImage = ImageIO.read(new File("A:\\Drive Locations\\Desktop\\ImageRecognition\\img1.jpg"));
//				//int x = 30, y = 947, w = 1380, h = 1395;
//			int x = 0, y = 0, w = 100, h = 100;
//			int matchcount = 0;
//			int nomatchcount = 0;
//			BufferedImage bi2 = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
//			bi2 = ImageIO.read(new File("C:\\SS\\hash" + imagetoCheck + ".png"));
//			
//			//BufferedImage image2 = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
//				int colorIntR = Integer.parseInt(colorToCheck.substring(0,2));
//				int colorIntG =  Integer.parseInt(colorToCheck.substring(3,5));
//				int colorIntB =  Integer.parseInt(colorToCheck.substring(6,8));
//				Color color = new Color(colorIntR, colorIntG, colorIntB); 
//				
//				
//				Color image1Color = (averageColor(bi2, x, y, w, h));
//				int image1R = image1Color.getRed();
//				int image1G = image1Color.getGreen();
//				int image1B = image1Color.getBlue();
//				
//				int image2R = color.getRed();
//				int image2G = color.getGreen();
//				int image2B = color.getBlue();
//				
//				int differenceR = Math.abs(image1R - image2R);
//				int differenceG = Math.abs(image1G - image2G);
//				int differenceB = Math.abs(image1B - image2B);
//				int tolerance = 10;
//		        
//				
//		        int lowerBoundR = image1R - tolerance;
//		        int upperBoundR = image1R + tolerance;
//		        
//		        int lowerBoundG = image1G - tolerance;
//		        int upperBoundG = image1G + tolerance;
//		        
//		        int lowerBoundB = image1B - tolerance;
//		        int upperBoundB = image1B + tolerance;
//		        
//		    
//		        if ((lowerBoundR <= image2R &&  image2R <= upperBoundR) && (lowerBoundG <= image2G &&  image2G <= upperBoundG) && (lowerBoundB <= image2B &&  image2B <= upperBoundB))  
//		        {
//		        	return true;
//		        }
//		        
//		        
//		 
//		 } catch (IOException e) {
//			e.printStackTrace();
//		}
//		 return false;
//		
//		}

	 
	 public static String getAverageColour(int imagetoCheck){
		 int x = 0, y = 0, w = 75, h = 75;
		 BufferedImage image1 = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
			try {
				//System.out.println("C:\\SS\\hash" + imagetoCheck  + ".png");
				image1 = ImageIO.read(new File("C:\\SS\\hash" + imagetoCheck  + ".png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			Color image1Color = (averageColor(image1, x, y, w, h));
			
			String R = Integer.toString(image1Color.getRed());
			String G = Integer.toString(image1Color.getGreen());;
			String B = Integer.toString(image1Color.getBlue());
			
			//System.out.println("RGB: " +R + ":" + G + ":" + B);
			
			String finalString = R + ":" + G + ":" + B;
					
			return finalString;
	 }
		
	public static void getMatches(int imagetoCheck){
		
		if(FirstRun){
			fillImages();
			FirstRun = false;
		}
		
		try {
			//Image loadedImage = ImageIO.read(new File("A:\\Drive Locations\\Desktop\\ImageRecognition\\img1.jpg"));
			//int x = 30, y = 947, w = 1380, h = 1395;
		int x = 0, y = 0, w = 75, h = 75;
		int matchcount = 0;
		int nomatchcount = 0;
		BufferedImage image1 = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		image1 = ImageIO.read(new File("C:\\SS\\hash" + imagetoCheck  + ".png"));
		
		BufferedImage image2 = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		
		for (int i = 1; i <= 36; i++) {
			String pathString = "C:\\SS\\Hash" + i  + ".png";
			image2 = ImageIO.read(new File(pathString));
			
			Color image1Color = (averageColor(image1, x, y, w, h));
			int image1R = image1Color.getRed();
			int image1G = image1Color.getGreen();
			int image1B = image1Color.getBlue();
			
			Color image2Color = (averageColor(image2, x, y, w, h));
			int image2R = image2Color.getRed();
			int image2G = image2Color.getGreen();
			int image2B = image2Color.getBlue();
			
			int differenceR = Math.abs(image1R - image2R);
			int differenceG = Math.abs(image1G - image2G);
			int differenceB = Math.abs(image1B - image2B);
			int tolerance = 10;
			
//	        System.out.println("Image 1 - R:" + image1R + " G:" + image1G + " B:" + image1B);
//	        System.out.println("Image " + i +"- R:" + image2R + " G:" + image2G + " B:" + image2B);
//	        System.out.println("");
//	        
//	        System.out.println("Difference - R:" + differenceR + " G:" + differenceG + " B:" + differenceB );
//	        System.out.println("");
			
	        int lowerBoundR = image1R - tolerance;
	        int upperBoundR = image1R + tolerance;
	        
	        int lowerBoundG = image1G - tolerance;
	        int upperBoundG = image1G + tolerance;
	        
	        int lowerBoundB = image1B - tolerance;
	        int upperBoundB = image1B + tolerance;
	        
	    
	        if ((lowerBoundR <= image2R &&  image2R <= upperBoundR) && (lowerBoundG <= image2G &&  image2G <= upperBoundG) && (lowerBoundB <= image2B &&  image2B <= upperBoundB))  
	        {
//	        	System.out.println("Match");
	        	matchedImages.add("hash" + i  + ".png");
	        	
	        	matchcount++;
	        }
	        else{
//	        	System.out.println("No match");
	        	nonMatchedImages.add("hash" + i  + ".png");
	        	nomatchcount++;
	        }
		}
		
		for (int i = 0; i < matchedImages.size(); i++) {
			Images.remove(matchedImages.get(i));
		}
		
		
	    System.out.println("Matches = " + matchcount );
	    System.out.println("No Matches = " + nomatchcount );
		
       //matchedImages.clear();
      // nonMatchedImages.clear();
	} catch (IOException e) {
		e.printStackTrace();
	}
	}
	
	public static Color averageColor(BufferedImage bi, int x0, int y0, int w,
	        int h) {
	    int x1 = x0 + w;
	    int y1 = y0 + h;
	    long sumr = 0, sumg = 0, sumb = 0;
	    for (int x = x0; x < x1; x++) {
	        for (int y = y0; y < y1; y++) {
	            Color pixel = new Color(bi.getRGB(x, y));
	            sumr += pixel.getRed();
	            sumg += pixel.getGreen();
	            sumb += pixel.getBlue();
	        }
	    }
	    int num = w * h;
	    float finalsumr = (sumr / num);
	    float finalsumg = (sumg / num);
	    float finalsumb = (sumb / num);
	    Color finalColor = new Color((int)finalsumr, (int)finalsumg, (int)finalsumb);
	    //String Final = "R " + (sumr / num) + " G " + ( sumg / num) + " B " + ( sumb / num);
	    return finalColor;
	}
}
